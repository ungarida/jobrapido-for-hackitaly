How to setup application on mac osx environmet.
--------------------------------------------------------
1) Go to http://www.eclipse.org/downloads/ and download the "Eclipse IDE for Java EE Developers" flavor of Eclipse

2) Install Eclipse

3) Open Eclipse, select you workspace (usually in /Users/YOURNAME/workspaces) and in "Help -> Eclipse Marketplace" search for "git" plugin and install EGit

4) Make a git clone of the project at this URL: https://bitbucket.org/ungarida/hackitaly in you workspace, for example mine is: /Users/giancarlo/workspaces/

5) Add Apache virtualhost as described here: http://foundationphp.com/tutorials/vhosts_leopard.php, you have to add as a virtualhost the webapp folder of the project
	
	5.1) As root user uncomment this line on /etc/apache2/httpd.conf
	
	# Virtual hosts
	Include /private/etc/apache2/extra/httpd-vhosts.conf
	
	5.2) As root user edit this file /private/etc/apache2/extra/httpd-vhosts.conf and put here the virtualhost configuration, for example this is mine:
	
	# Base Server root re-definition
	<VirtualHost *:80>
	   DocumentRoot "/Library/WebServer/Documents"
	   ServerName localhost
	</VirtualHost>
	
	<VirtualHost *:80>
	   DocumentRoot "/Users/giancarlo/workspaces/hackitaly/hackrapido/src/main/webapp"
	   ServerName hack.jobrapido.com
	
	# Hrapido virtual host
	<Directory "/Users/giancarlo/workspaces/hackitaly/hackrapido/src/main/webapp">
	     Options Indexes FollowSymLinks MultiViews
	     AllowOverride All
	     Order allow,deny
	     Allow from all
	</Directory>

	RewriteEngine  on
	RewriteRule     /api/(.*)    http://hack.jobrapido.com:10000/$1 [P]

	</VirtualHost>

		
	5.3) Restart apache with this command: sudo apachectl restart
	
6) Run the project whitin Eclipse (without build any jar), go to com.jobrapido.hack.Main.java and right click with the mouse and select "Run as java application"

7) Go to http://hack.jobrapido.com and Enjoy Hrapido!! :)