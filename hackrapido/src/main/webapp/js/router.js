define([ 'jquery', 'underscore', 'backbone', 'views/home.page' , 'views/search.page' ], function($, _, Backbone,ViewHomePage,ViewSearchPage) {
	var self = this;
	var AppRouter = Backbone.Router.extend({
		routes : {
			"*actions" : "dispatcher"
		},

		dispatcher: function(action) {
			console.log("dispatcher",action);
			var _self = this;
			if ("search" == action) {
				$.ajax("http://hack.jobrapido.com/api/isauthorized",{"success":
					function(data, textStatus, jqXHR){
						console.log("isauthorized",data)
						if (data.authorization) {
							_self.navigate("search");
							_self.search();
						} else {
							_self.navigate("login");
							_self.login();
						}
					}
				});
			} else {
				if ("login" == action) {
					_self.login();
				} else {
					_self.home();
				}
			}
		},

		home : function() {
			console.log("home");
			this.viewHomePage = new ViewHomePage();
			this.viewHomePage.render();
		},
		
		search : function() {
			console.log("search");
			this.viewSearchPage = new ViewSearchPage();
			this.viewSearchPage.render();
		},
		
		login: function() {
			console.log("login");
			console.log("AppRouter|login");
			window.location.replace("/api/authrequest");
		}
	});

	var initialize = function() {
		this.router = new AppRouter;
		Backbone.history.start();
	};

	return {
		initialize : initialize
	};
});