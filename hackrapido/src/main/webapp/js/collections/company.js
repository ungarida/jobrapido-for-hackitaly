define([ 'jquery', 'underscore', 'backbone', 'models/company' ], function($, _, Backbone, modelCompanyList) {
	var CompanyCollection = Backbone.Collection.extend({
		model : modelCompanyList,
		initialize : function() {
		},
		setWhat: function(what) {
			this.what = what;
		},
		setLocation: function(location) {
			if (location == "") location = "us";
			this.location = location;
		},
		url : function() {
			return "http://hack.jobrapido.com/api/company/"+this.location+"/"+this.what;
		}
	});
	return new CompanyCollection;
});