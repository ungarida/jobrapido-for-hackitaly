define([ 'jquery', 'underscore', 'backbone', 'models/jobseeker' ], function($, _, Backbone, JobSeekerModel) {
	var UserCollection = Backbone.Collection.extend({
		model : JobSeekerModel,
		initialize : function() {
		},
		url : "http://hack.jobrapido.com/api/jobseeker/"
	});
	return new UserCollection;
});