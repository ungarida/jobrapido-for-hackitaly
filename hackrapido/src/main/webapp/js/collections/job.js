define([ 'jquery', 'underscore', 'backbone', 'models/job' ], function($, _, Backbone, modelJobList) {
	var JobCollection = Backbone.Collection.extend({
		model : modelJobList,
		setWhat: function(what) {
			this.what = what;
		},
		setLocation: function(location) {
			this.location = location;
		},
		url : function() {
			return "http://us.jobrapido.com/?do=api.feed&login=hackitaly&password=h4ck-m3&w="+this.what+"&l="+this.location+"&format=json&callback=?"
		},
		initialize : function() {
		},
		parse : function(response) {
			return response.searchResults.results;
		}
	});
	return new JobCollection;
});