define([ 'jquery', 'underscore', 'backbone', 'text!templates/loading.data.txt'],
	function($, _, Backbone, loadingTemplate) {
		var ViewLoading = Backbone.View.extend({
			tagName:"div", 
			initialize : function(description) {
				_.bindAll(this, "render");
				_.bindAll(this, "kill");
				this.description = description;
			},
			render : function() {
				console.log("ViewLoading|render");
				this.$el.html(_.template(loadingTemplate, {"description":this.description}));
				var progress = $(".progress",this.$el);
				console.log("progress",progress);
				this.timerProgressBar = setInterval(function(){
					var maxWidth = progress.width();
					var bar = $(".bar",progress);
					var width = $(".bar",progress).width();
					if (width < (maxWidth-20)) {
						width += 20;
					} else {
						width = 0;
					}
					bar.width(width);
				},100);
				return this.$el;
			},
			kill : function() {
				console.log("ViewLoading|kill");
				clearTimeout(this.timerProgressBar);
				this.$el.remove();
			}
		});
		return ViewLoading;
	});