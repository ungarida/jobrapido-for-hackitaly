define([ 'jquery', 'underscore', 'backbone', 'text!templates/company.item.data.txt' ], function($, _, Backbone,
		companyItemDataTemplate) {
	var ViewCompanyItem = Backbone.View.extend({
		tagName : 'li',
		initialize : function() {
			this.map = this.options.map;
			_.bindAll(this, "render");
			_.bindAll(this, "addMarker");
			_.bindAll(this, "autoCenter");
			this.markIconImages = [ "http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png",
					"http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png",
					"http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png",
					"http://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png",
					"http://www.google.com/intl/en_us/mapfiles/ms/micons/purple-dot.png",
					"http://www.google.com/intl/en_us/mapfiles/ms/micons/orange-dot.png", ]
		},

		render : function() {
			console.log("ViewCompanyItem|render", this.model);
			var companyName = this.model.get("name");
			var _self = this;
			$.ajax("http://hack.jobrapido.com/api/linkedin-company/" + companyName, {
				"success" : function(response) {
					if (response && response != "") {
						_self.$el.append(_.template(companyItemDataTemplate, {
							"JRModel" : _self.model,
							"LIResponse" : response
						}));
						var advertsContainer = $(".adverts", _self.$el);
						advertsContainer.hide();

						$(".adverts-toggler", _self.$el).bind("click", function() {
							advertsContainer.slideToggle('fast', function() {
							})
						});
						$(".website", _self.$el).bind("click", function() {
							window.location.replace(response.websiteUrl)
						});
						$(".twitter", _self.$el).bind("click", function() {
							window.location.replace("http://twitter.com/" + response.twitterId)
						});

						var companyName = response.name;
						var locations = response.locations.values;
						if (locations && locations.length > 0) {
							var location = locations[0];
							console.log("ViewCompanyItem|location", location);
							var address = "";
							if (location.address.street1)
								address += location.address.street1 + " , ";
							if (location.address.city)
								address += location.address.city + " , ";
							if (location.address.postalCode)
								address += location.address.postalCode;
							if (address != "") {
								var icon = _self.markIconImages[_self.options.index];
								_self.addMarker(companyName, address, icon);
							}
						}
					}
				}
			});

			$.ajax("http://hack.jobrapido.com/api/twitter-mentions-counter/" + companyName, {
				"success" : function(response) {
					var setBadgeTwitterMentions = function(target, value, iteration) {
						console.log("setBadgeTwitterMentions|" + iteration, target);
						var badge = $(".badge.twitter-mentions", target);
						if (badge.length > 0) {
							badge.text(value);
							if (value == "+15") {
								badge.addClass("badge-success");
							} else if (value == "0") {
								badge.addClass("badge-important");
							} else {
								badge.addClass("badge-warning");
							}
						} else {
							if (iteration < 25) {
								iteration += 1
								setTimeout(function() {
									setBadgeTwitterMentions(target, value, iteration)
								}, 200);
							}
						}
					}

					var mentions = 0;
					if (response && response.count && response.count != "") {
						mentions = response.count;
					}
					setBadgeTwitterMentions(_self.$el, mentions, 0);
				}
			});

			return this.$el
		},

		addMarker : function(companyName, address, icon) {
			var map = this.map;
			var request = {
				"location" : map.getCenter(),
				"radius" : 5000,
				"keyword" : companyName
			};
			var address = address;
			var geocoder = new google.maps.Geocoder();
			var _self = this;
			geocoder.geocode({
				'address' : address
			}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					map.setCenter(results[0].geometry.location);
					var marker = new google.maps.Marker({
						"animation" : google.maps.Animation.DROP,
						"icon" : icon,
						"map" : map,
						"position" : results[0].geometry.location,
						"title" : companyName
					});
					window.hrSearchMapMarkers.push(marker);
					_self.autoCenter(marker);

					google.maps.event.addListener(marker, 'mouseover', function(event) {
						_self.$el.removeClass("sendBack");
						_self.$el.addClass("sendFront");
					});

					google.maps.event.addListener(marker, 'mouseout', function(event) {
						_self.$el.removeClass("sendFront");
						_self.$el.addClass("sendBack");
					});

				} else {
					console.log("Geocode was not successful for the following reason: " + status);
				}
			});
		},

		autoCenter : function(marker) {
			var bounds = new google.maps.LatLngBounds();
			var markers = window.hrSearchMapMarkers;
			console.log("ViewSearchForm|autoCenter", markers);
			if (markers) {
				for ( var i = 0; i < markers.length; i++) {
					var marker = markers[i];
					console.log("ViewSearchForm|autoCenter|marker", marker);
					bounds.extend(marker.getPosition());
				}
				this.map.fitBounds(bounds);
			}
		}
	});
	return ViewCompanyItem;
});