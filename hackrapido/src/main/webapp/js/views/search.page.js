define([ 'jquery', 'underscore', 'backbone', 'collections/company', 'views/search.jobseeker',  'views/search.form', 'views/search.map', 'text!templates/search.page.html'],
		function($, _, Backbone, companyList, ViewSearchJobSeeker, ViewSearchForm, ViewSearchMap, searchPageTemplate) {
			var ViewSearchPage = Backbone.View.extend({
				el : ".page",
				initialize : function() {
					_.bindAll(this, "render");
					window.hrSearchMapMarkers = [];
					//http://maps.googleapis.com/maps/api/geocode/json?address=US&sensor=true
					this.usMap = {
						"bounds" : {
							"southWest": new google.maps.LatLng(71.3898880,-66.94976079999999),
							"northEast": new google.maps.LatLng(18.91106420, 172.45469660)
						},
						"location": new google.maps.LatLng(37.090240,-95.7128910)
					}
				},
				render : function() {
					console.log("ViewSearchPage|render",this.$el);
					this.$el.html(searchPageTemplate);
					
					this.viewSearchJobSeeker = new ViewSearchJobSeeker({"el": $("#jobseeker",this.$el)});
					this.viewSearchJobSeeker.render();

					this.viewSearchMap = new ViewSearchMap({"el": $("#map",this.$el), "usMap": this.usMap});
					this.viewSearchMap.render();
					
					this.viewSearchForm = new ViewSearchForm({"el": $("#search",this.$el), "usMap": this.usMap, "viewSearchMap": this.viewSearchMap});
					this.viewSearchForm.render();
				},
			});
			return ViewSearchPage;
		});