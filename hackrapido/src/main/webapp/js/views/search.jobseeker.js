define([ 'jquery', 'underscore', 'backbone', 'collections/jobseeker', 'views/loading', 'text!templates/search.jobseeker.html','text!templates/search.jobseeker.data.txt'],
	function($, _, Backbone, jobseekerStore, ViewLoading, searchJobSeekerTemplate, searchJobSeekerDataTemplate) {
		var ViewSearchJobSeeker = Backbone.View.extend({
			initialize : function() {
				_.bindAll(this, "render");
				_.bindAll(this, "showData");
				_self = this;
				jobseekerStore.fetch({
					success : function() {
						console.log("ViewSearchJobSeeker|fetch");
						_self.showData();
					}
				});
				this.collection = jobseekerStore;
			},
			render : function() {
				console.log("ViewSearchJobSeeker|render",this.$el);
				this.loading = new ViewLoading();
				this.loading.initialize("Retrieving Linkedin account data...");
				var container = this.$el.html(_.template(searchJobSeekerTemplate));
				this.loading.render().appendTo($(":first-child",container));
				return this.$el;
			},
			showData : function() {
				var model = this.collection.models[0];
				console.log("ViewSearchJobSeeker|showData",model);
				this.loading.kill();
				this.$el.html(_.template(searchJobSeekerDataTemplate, {"model":model}));
				var skillButtons = $("button",this.$el);
				console.log("skillButtons",skillButtons);
				skillButtons.bind("click",function(){
					console.log("click on skill:"+$(this).text());
					var skill = $(this).text();
					$("#w").val(skill);
					$("#search>form").submit();
				});
				
			}
		});
		return ViewSearchJobSeeker;
	});