define([ 'jquery', 'underscore', 'backbone' ], function($, _, Backbone) {
	var ViewJobItem = Backbone.View.extend({
		tagName : 'li',
		initialize : function() {
			
		},
		render : function() {
			console.log("ViewJobItem|render", this.model);
			var link = $("<a>");
			link.attr("href", this.model.get("url"));
			link.text(this.model.get("company"));
			return $(this.el).append(link);
		}
	});
	return ViewJobItem;
});