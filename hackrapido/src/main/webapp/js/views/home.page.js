define([ 'jquery', 'underscore', 'backbone', 'text!templates/home.page.html'],
function($, _, Backbone, homePageTemplate) {
	var ViewHomePage = Backbone.View.extend({
		el : ".page",
		initialize : function() {
			_.bindAll(this, "render");
		},
		
		render : function() {
			this.$el.html(homePageTemplate);
		},
	});
	return ViewHomePage;
});