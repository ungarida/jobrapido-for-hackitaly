define([ 'jquery', 'underscore', 'backbone', 'collections/company', 'views/loading', 'views/search.map', 'views/companylist', 'text!templates/search.form.html',
		'async!http://maps.google.com/maps/api/js?key=AIzaSyACZnvG2tQrYMA6InedFKI3lVU1gDodnvI&libraries=places&sensor=false' ],
		function($, _, Backbone, companyList, ViewLoading, ViewSearchMap, ViewCompanyList, searchFormTemplate) {
			var ViewSearchForm = Backbone.View.extend({
				events : {
					"submit form" : "submit"
				},
				initialize : function() {
					_.bindAll(this, "render");
					_.bindAll(this, "submit");
					_.bindAll(this, "resetMap");
					this.usMap = this.options.usMap;
					this.viewSearchMap = this.options.viewSearchMap;
				},
				render : function() {
					console.log("ViewSearchForm|render",this.$el);
					this.$el.html(searchFormTemplate);
					
					this.what = $("input[name=w]");
					this.location = $("input[name=l]");

					var unitedStatesBounds = new google.maps.LatLngBounds(this.usMap.bounds.southWest, this.usMap.bounds.northEast );
							
					var options = {
						bounds : unitedStatesBounds,
						componentRestrictions: { country: "us" },
						types : ["(cities)"]
					};
					var autocomplete = new google.maps.places.Autocomplete(this.location.get(0), options);
					var viewSearchMap = this.viewSearchMap;
					google.maps.event.addListener(autocomplete, 'place_changed', function() {
						console.log("ViewSearchForm|autocomplete",autocomplete);
						place = autocomplete.getPlace();
						viewSearchMap.getMap().setCenter(place.geometry.location);
						viewSearchMap.getMap().setZoom(10);
					});
					return this.$el;
				},
				submit : function(event) {
					console.log("ViewSearchForm|submit",event);
					event.preventDefault();
					
					this.loading = new ViewLoading();
					this.loading.initialize("Searching companies...");
					$("#results").html(this.loading.render());
					var viewSearchMap = this.viewSearchMap;
					var map = viewSearchMap.getMap();
					
					var viewCompanyList = new ViewCompanyList({
						"collection" : companyList,
						"map": map
					});
					
					var loading = this.loading;
					companyList.setWhat($(this.what).val());
					companyList.setLocation($(this.location).val());
					var _self = this;
					this.resetMap();
					companyList.fetch({
						success : function() {
							console.log("ViewSearchForm|fetch");
							loading.kill();
							viewCompanyList.render();
						}
					});

					return false;
				},
				resetMap: function() {
					var markers=window.hrSearchMapMarkers;
					console.log("ViewSearchForm|resetMap",markers);
					if(markers) {
						for (var i=0;i<markers.length;i++) {
							var marker = markers[i];
							marker.setMap(null);
						}
						this.viewSearchMap.resetMap();
					}
					window.hrSearchMapMarkers = [];
				}
			});
			return ViewSearchForm;
		});