define([ 'jquery', 'underscore', 'backbone', 'views/companyitem' ], function($, _, Backbone, ViewCompanyItem) {
	var ViewCompanyList = Backbone.View.extend({
		el : "#results",

		initialize : function() {
			_.bindAll(this, "render");
			this.viewListSize = 6;
		},

		render : function() {
			console.log("ViewCompanyList|render",this);
			this.$el.html("");
			var list = $("<ul>");
			var counter = 0;
			var markers = this.options.markers;
			var map = this.options.map;
			
			_(this.collection.models).each(function(item, index) {
				if (counter<this.viewListSize) {
					var viewCompanyItem = new ViewCompanyItem({
						"model" : item,
						"map" : map,
						"index" : index
					});
					list.append(viewCompanyItem.render());
					counter+=1;
					console.log("ViewJobList1|"+counter);
				}
			}, this);
			this.$el.append(list);
			return this;
		}
		
	});
	return ViewCompanyList;
});