define([ 'jquery', 'underscore', 'backbone', 'text!templates/search.map.html'],
		function($, _, Backbone, searchMapTemplate) {
			var ViewSearchMap = Backbone.View.extend({
				initialize : function() {
					_.bindAll(this, "render");
					_.bindAll(this, "getMap");
					_.bindAll(this, "resetMap");
					this.usMap = this.options.usMap;
					this.location = this.usMap.location;
					this.map;
				},
				render : function() {
					console.log("ViewSearchMap|render",this.$el);
					this.$el.html(searchMapTemplate);
					window.console.log("canvas",$(".map_canvas",this.$el));
					this.map = new google.maps.Map($(".map_canvas",this.$el)[0], {
						disableDefaultUI: true,
					    mapTypeId: google.maps.MapTypeId.ROADMAP,
					    center: this.location,
					    zoom: 3
					});
					
					return this.$el;
				},
				getMap: function() {
					return this.map;
				},
				resetMap: function() {
					this.map.setCenter(this.location);
					this.map.setZoom(3);
				}
			});
			return ViewSearchMap;
		});