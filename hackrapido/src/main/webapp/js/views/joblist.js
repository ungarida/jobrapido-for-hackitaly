define([ 'jquery', 'underscore', 'backbone', 'views/jobitem' ], function($, _, Backbone, ViewJobItem) {
	var ViewJobList = Backbone.View.extend({
		el : "#results",

		initialize : function() {
			_.bindAll(this, "render");
			this.viewListSize = 5;
		},

		render : function() {
			console.log("ViewJobList1|render",this.$el);
			$(this.$el).html("");
			var list = $("<ul>");
			var counter = 0;
			_(this.collection.models).each(function(item) {
				if (counter<this.viewListSize) {
					var viewJobItem = new ViewJobItem({
						model : item
					});
					list.append(viewJobItem.render());
					counter+=1;
					console.log("ViewJobList1|"+counter);
				}
			}, this);
			$(this.$el).append(list);
			return this;
		}
	});
	return ViewJobList;
});