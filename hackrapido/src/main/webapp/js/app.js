define([ 'jquery', 'underscore', 'backbone', 'bootstrap', 'view', 'views/app', 'events', 'router' ], function($, _, Backbone,
		Bootstrap, View, AppView, Events, Router) {
	var initialize = function() {
		var appView = View.create({}, 'AppView', AppView);
		appView.render();
		Router.initialize({appView : appView});
	}

	return {
		initialize : initialize
	};
});