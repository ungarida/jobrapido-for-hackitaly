require.config({
	paths : {
		loader : 'libs/loader',
		jquery : 'libs/jquery-1.7.2.min',
		underscore : 'libs/underscore-1.3.3.min',
		backbone : 'libs/backbone-0.9.2.min',
		text : 'libs/text',
		bootstrap : 'libs/bootstrap-2.0.4.min',
		async: 'libs/requirejsplugins/async',
		goog: 'libs/requirejsplugins/goog',
		templates : '../templates'
	}
});

require([ 'app' ], function(App) {
	App.initialize();
});