<div class="span1">
	<img src="<%= model.get("photo") %>" />
</div>
<div class="span10">
	<p class="description">Welcome</p>
	<h2><%= model.get("name") %> <%= model.get("surname") %>!</h2>
	<p><%= model.get("headline") %></p>
	<ul class="skills">
	<% var skills = model.get("skills"); %>
    <% for (var i = 0; i < skills.length; i++) { %>
      <% var skill = skills[i]; %>
      <li>
        <button class="btn btn-mini btn-info"><%= skill %></button>
      </li>
    <% } %>
    </ul>
</div>
