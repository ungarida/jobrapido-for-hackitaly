<% 
var adverts = JRModel.get("adverts");
var logoUrl = LIResponse.squareLogoUrl;
if (!logoUrl) logoUrl = "img/company.png";
function formatDate(date) {
	return date.substring(7, 8) + "/" + date.substring(5, 6) + "/" + date.substring(0, 4);
}
function formatAddress(response) {
	var locations = response.locations.values;
	var address = "";
	if (locations && locations.length>0) {
		var location = locations[0];
		if (location.address.street1) address+=location.address.street1 + " , ";
		if (location.address.city) address+=location.address.city +" , ";
		if (location.address.postalCode) address+=location.address.postalCode;
	}
	return address;
}
%>
<div class="row-fluid header">
	<div class="span2">
		<div class="logo" style="background-image:url('<%= logoUrl %>');"></div>
 	</div>
	<div class="span5">
		<h2 class="name"><%= JRModel.get("name") %></h2>
		<p class="address"><%= formatAddress(LIResponse) %></p>
	</div>
	<div class="span1">
		<span class="badge twitter-mentions"></span>
	</div>
	<div class="span1">
		<button class="btn twitter"></button>
	</div>
	<div class="span1">
		<button class="btn website"></button>
	</div>
	<div class="span2">
		<button class="btn btn-warning adverts-toggler"><%= adverts.length %> job offers</button>
	</div>
</div>

<div class="row-fluid content">
<ul class="adverts">
<% for (var i = 0; i < adverts.length; i++) { %>
  <% var advert = adverts[i]; %>
  <li>
   <%= formatDate(advert.date) %> - <a href="<%= advert.url %>" target="_blank"><%= advert.title %></a>
  </li>
<% } %>
</ul>
</div>