package com.jobrapido.hack;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.jobrapido.hack.rest.HackItalyRESTModule;
import com.jobrapido.hack.rest.server.JerseyRESTServer;

public class Main {
	
	static Logger logger = Logger.getLogger(Main.class);
	
    public static void main(String[] args){
    	
    	// Set up a simple configuration that logs on the console.
        BasicConfigurator.configure();
        
        Injector injector = Guice.createInjector(new HackItalyRESTModule());
        logger.info("Starting..");
        
        // Start Grizzly server
        JerseyRESTServer restServer = injector.getInstance(JerseyRESTServer.class);
        restServer.init();
    }
    
}
