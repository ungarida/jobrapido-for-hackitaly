package com.jobrapido.hack.rest.entities.linkedin;

import java.util.List;

public class LICompany {
	private String id;
	private String name;
	private String universalName;
	private String websiteUrl;
	private String description;
	
	private LICompanyType companyType;
	private LICompanyStockExchange stockExchange;
	private LICompanyEmployeeCountRange employeeCountRange;
	private String numFollowers;
}


class LICompanyType{
	private String code;
	private String name;
}

class LICompanyStockExchange{
	private String code;
	private String name;
}

class LICompanyEmployeeCountRange{
	private String code;
	private String name;
}

class LICompanyLocations{
	private int _total;
	private List<LICompanyLocation> values;
}

class LICompanyLocation{
	private String description;
	private boolean isHeadquarters;
	private LICompanyAddress address;
}

class LICompanyAddress{
	private String city;
	private String postalCode;
	private String street1;
}