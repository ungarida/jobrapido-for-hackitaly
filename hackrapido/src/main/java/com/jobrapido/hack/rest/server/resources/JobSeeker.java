package com.jobrapido.hack.rest.server.resources;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.jobrapido.hack.rest.utils.DataProvider;

@Path("/jobseeker")
public class JobSeeker {
	
	@GET
	@Produces("application/json")
	public String getMyLinkedinProfileInfo(
			@CookieParam("objectId") String objectId) {
		return DataProvider.getMyLinkedinProfileInfo(objectId);
	}
	
	@GET
	@Path("{name}/{surname}")
	@Produces("application/json")
	public String searchLinkedinUserInfo(
			@CookieParam("objectId") String objectId, 
			@PathParam("name") String name, 
			@PathParam("surname") String surname) {
		return DataProvider.searchLinkedinUserProfile(objectId, name, surname);
	}
	
}
