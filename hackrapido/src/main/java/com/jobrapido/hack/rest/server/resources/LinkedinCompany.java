package com.jobrapido.hack.rest.server.resources;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.jobrapido.hack.rest.utils.DataProvider;

@Path("/linkedin-company")
public class LinkedinCompany {

	@GET
	@Path("{name}")
    @Produces("application/json")
    public String getLinkedinCompanyDetails(
			@CookieParam("objectId") String objectId, 
			@PathParam("name") String name) {
		
		return DataProvider.getLinkedinCompanyDetails(objectId, name);
    }
	
}
