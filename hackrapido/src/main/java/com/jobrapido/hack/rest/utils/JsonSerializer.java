package com.jobrapido.hack.rest.utils;

import java.util.List;

import com.google.gson.Gson;
import com.jobrapido.hack.rest.entities.jobrapido.JRApiResponseObj;
import com.jobrapido.hack.rest.entities.linkedin.LICompaniesSearchResult;
import com.jobrapido.hack.rest.entities.linkedin.LICompany;
import com.jobrapido.hack.rest.entities.linkedin.LIUser;
import com.jobrapido.hack.rest.entities.parse.JobSeeker;
import com.jobrapido.hack.rest.entities.parse.ObjectCreatedResponse;
import com.jobrapido.hack.rest.entities.twitter.TwitterMentionCount;
import com.jobrapido.hack.rest.entities.twitter.TwitterSearchResult;

public class JsonSerializer {
	
	public static <T> String toJson(List<T> objs){
		if(objs == null) return "";
		return new Gson().toJson(objs);
	}
	
	public static JRApiResponseObj getJRApiResponseObjFromJson(String json){
		return new Gson().fromJson(json, JRApiResponseObj.class);
	}

	public static String toJsonJobSeeker(
			com.jobrapido.hack.rest.entities.JobSeeker js) {
		return new Gson().toJson(js);
	}
	
	// PARSE RELATED
	public static String toParseJobSeeker(JobSeeker jobSeeker){
		return new Gson().toJson(jobSeeker);
	}
	
	public static JobSeeker fromParseJobSeeker(String json){
		return new Gson().fromJson(json, JobSeeker.class);
	}
	
	public static ObjectCreatedResponse fromParseObjectCreated(String json){
		return new Gson().fromJson(json, ObjectCreatedResponse.class);
	}
	// PARSE RELATED - END
	
	
	
	
	// LINKEDIN RELATED
	public static LIUser fromLinkedinUser(String json){
		return new Gson().fromJson(json, LIUser.class);
	}
	
	public static LICompany fromLinkedinCompany(String json){
		return new Gson().fromJson(json, LICompany.class);
	}
	
	public static LICompaniesSearchResult fromLinkedinCompanySearch(String json){
		return new Gson().fromJson(json, LICompaniesSearchResult.class);
	}
	// LINKEDIN RELATED - END
	
	
	// TWITTER RELATED
	public static TwitterSearchResult fromTwitterSearch(String json){
		return new Gson().fromJson(json, TwitterSearchResult.class);
	}
	
	public static String toTwitterSearch(TwitterSearchResult twitterSearchResult){
		return new Gson().toJson(twitterSearchResult);
	}
	
	public static String toTwitterMentionCount(TwitterMentionCount twitterSearchResult){
		return new Gson().toJson(twitterSearchResult);
	}
	// TWITTER RELATED - END
}
