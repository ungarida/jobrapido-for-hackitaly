package com.jobrapido.hack.rest.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.jobrapido.hack.rest.client.JobrapidoRESTClient;
import com.jobrapido.hack.rest.client.LinkedinRESTClient;
import com.jobrapido.hack.rest.client.TwitterRESTClient;
import com.jobrapido.hack.rest.entities.Company;
import com.jobrapido.hack.rest.entities.JobAdvert;
import com.jobrapido.hack.rest.entities.JobSeeker;
import com.jobrapido.hack.rest.entities.jobrapido.JRApiResponseObj;
import com.jobrapido.hack.rest.entities.jobrapido.JRSearchResultItem;
import com.jobrapido.hack.rest.entities.linkedin.LICompaniesSearchResult;
import com.jobrapido.hack.rest.entities.linkedin.LICompany;
import com.jobrapido.hack.rest.entities.linkedin.LIUser;
import com.jobrapido.hack.rest.entities.twitter.TwitterMentionCount;
import com.jobrapido.hack.rest.entities.twitter.TwitterSearchResult;

public class DataProvider {

	@Inject private static JobrapidoRESTClient jobrapidoRESTClient;
	@Inject private static LinkedinRESTClient linkedinRESTClient;
	@Inject private static TwitterRESTClient twitterRESTClient;
	
	static Logger logger = Logger.getLogger(DataProvider.class);

	
	// JOBRAPIDO RELATED
	public static String getJobAdvertsFromJobrapido(String location, String what) {

		List<JobAdvert> jobAdvertList = new ArrayList<JobAdvert>();

		String jrResponseString = jobrapidoRESTClient.searchJobAdvertsWhatLocation(location, what);
		JRApiResponseObj jrApiResponseObj = JsonSerializer.getJRApiResponseObjFromJson(jrResponseString);
		List<JRSearchResultItem> results = jrApiResponseObj.getSearchResults().getResults();
		for (JRSearchResultItem jrSearchResultItem : results) {
			jobAdvertList.add(convertJrSearchResultItemToJobAdvert(jrSearchResultItem));
		}
		return JsonSerializer.toJson(jobAdvertList);
	}

	private static JobAdvert convertJrSearchResultItemToJobAdvert(JRSearchResultItem jrSearchResultItem) {
		return new JobAdvert(jrSearchResultItem.getId(), jrSearchResultItem.getTitle(), "",
				jrSearchResultItem.getLocation(), jrSearchResultItem.getCompany(), jrSearchResultItem.getDate(),
				jrSearchResultItem.getWebsite(), jrSearchResultItem.getUrl());
	}

	public static String getCompaniesFromJobrapido(String location, String what) {
		Map<String, Company> companiesStore = new HashMap<String, Company>();

		String jrResponseString = jobrapidoRESTClient.searchJobAdvertsWhatLocation(location, what);
		System.out.println(jrResponseString);
		JRApiResponseObj jrApiResponseObj = JsonSerializer.getJRApiResponseObjFromJson(jrResponseString);
		List<JRSearchResultItem> results = jrApiResponseObj.getSearchResults().getResults();
		if (results != null) {
			for (JRSearchResultItem jrSearchResultItem : results) {
				String companyName = jrSearchResultItem.getCompany();
				if ((companyName != null) && (companyName.length()>0)){
					Company company;
					if (companiesStore.containsKey(companyName)) {
						company = companiesStore.get(companyName);
					} else {
						company = new Company(companyName);
					}
					company.addJobAdvert(convertJrSearchResultItemToJobAdvert(jrSearchResultItem));
					companiesStore.put(companyName, company);
				}
			}
			ArrayList<Company> companies = new ArrayList<Company>(companiesStore.values());
			return JsonSerializer.toJson(companies);
		} else {
			return "{}";
		}

	}
	// JOBRAPIDO RELATED - END
	
	
	
	
	// LINKEDIN RELATED
	public static String getMyLinkedinProfileInfo(String objectId) {
		String linkedinUserProfileInfo = linkedinRESTClient.getMyLinkedinProfileInfo(objectId);
		LIUser fromLinkedinUser = JsonSerializer.fromLinkedinUser(linkedinUserProfileInfo);
		com.jobrapido.hack.rest.entities.JobSeeker js = new JobSeeker(fromLinkedinUser);
		String result = JsonSerializer.toJsonJobSeeker(js);
		return result;
	}

	public static String searchLinkedinUserProfile(String objectId, String name, String surname) {
		String linkedinUserProfileInfo = linkedinRESTClient.userProfileSearchByNameAndSurname(objectId, name, surname);
		return linkedinUserProfileInfo;
	}

	public static String searchLinkedinCompanies(String objectId, String name) {
		String linkedinCompaniesProfileInfo = linkedinRESTClient.companiesProfileSearchByName(objectId, name);
		return linkedinCompaniesProfileInfo;
	}
	
	
	public static String searchLinkedinCompanies(String objectId, String name, String location) {
		if (StringUtils.isBlank(location) || location.equals("us")){
			return  getLinkedinCompanyDetails(objectId, name);
		}else{
			String companiesProfileSearchByName = linkedinRESTClient.companiesProfileSearchByName(objectId, name);
			LICompaniesSearchResult fromLinkedinCompanySearch = JsonSerializer.fromLinkedinCompanySearch(companiesProfileSearchByName);
			String liCompanySearchResultCandidateIdLocationBased = fromLinkedinCompanySearch.getLICompanySearchResultCandidateIdLocationBased(location);

			String companyProfileById = linkedinRESTClient.getCompanyProfileById(objectId, liCompanySearchResultCandidateIdLocationBased);
			LICompany fromLinkedinCompany = JsonSerializer.fromLinkedinCompany(companyProfileById);
			return companyProfileById;
		}
	}
	
	
	public static String getLinkedinCompanyDetails(String objectId, String name){
		String companiesProfileSearchByName = linkedinRESTClient.companiesProfileSearchByName(objectId, name);
		LICompaniesSearchResult fromLinkedinCompanySearch = JsonSerializer.fromLinkedinCompanySearch(companiesProfileSearchByName);
		String liCompanySearchResultCandidateId = fromLinkedinCompanySearch.getLICompanySearchResultCandidateId();

		String companyProfileById = linkedinRESTClient.getCompanyProfileById(objectId, liCompanySearchResultCandidateId);
		LICompany fromLinkedinCompany = JsonSerializer.fromLinkedinCompany(companyProfileById);
		return (companyProfileById!=null) ? companyProfileById : "";
	}
	// LINKEDIN RELATED - END
	
	
	
	// TWITTER RELATED
	public static String searchTwitterForTerm(String searchTerm){
		String searchForTweetAbout = twitterRESTClient.searchForTweetAbout(searchTerm);
		TwitterSearchResult fromTwitterSearch = JsonSerializer.fromTwitterSearch(searchForTweetAbout);
		return JsonSerializer.toTwitterSearch(fromTwitterSearch);
	}
	
	public static String getTwitterMentionsCountLastWeek(String company) {
		String searchForTweetAboutMention = twitterRESTClient.searchForTweetAboutMention(company);
		TwitterSearchResult fromTwitterSearch = JsonSerializer.fromTwitterSearch(searchForTweetAboutMention);
		TwitterMentionCount tmc = new TwitterMentionCount();
		tmc.setCompany(company);
		tmc.setCount(fromTwitterSearch.getLastDayTweetsCount());
		return JsonSerializer.toTwitterMentionCount(tmc);
	}
	// TWITTER RELATED - END
}
