package com.jobrapido.hack.rest.entities.linkedin;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class LICompaniesSearchResult {
	private LICompaniesSearchResultList companies;
	
	public String getLICompanySearchResultCandidateId(){
		if (getCompanies().getValues()!= null)
		return getCompanies().getValues().get(0).getId();
		else
		return null;
	}
	
	public String getLICompanySearchResultCandidateIdLocationBased(String city){
		List<LICompanySearchResult> values = getCompanies().getValues();
		for (LICompanySearchResult liCompanySearchResult : values) {
			List<LICompanySearchResultLocation> values2 = liCompanySearchResult.getLocations().getValues();
			if(values2 != null){
				for (LICompanySearchResultLocation liCompanySearchResultLocation : values2) {
					String city2 = liCompanySearchResultLocation.getAddress().getCity();
					if(StringUtils.isNotBlank(city2) && city2.equals(city)) return liCompanySearchResult.getId();
				}
			}
		}
		return getLICompanySearchResultCandidateId();
	}
	
	
	public LICompaniesSearchResultList getCompanies() {
		return companies;
	}

	public void setCompanies(LICompaniesSearchResultList companies) {
		this.companies = companies;
	}
}

class LICompaniesSearchResultList {
	private int _total;
	private int _start;
	private int _count;
	
	private List<LICompanySearchResult> values;
	
	public int get_total() {
		return _total;
	}
	public void set_total(int _total) {
		this._total = _total;
	}
	public List<LICompanySearchResult> getValues() {
		return values;
	}
	public void setValues(List<LICompanySearchResult> values) {
		this.values = values;
	}
	public int get_start() {
		return _start;
	}
	public void set_start(int _start) {
		this._start = _start;
	}
	public int get_count() {
		return _count;
	}
	public void set_count(int _count) {
		this._count = _count;
	}
}


class LICompanySearchResult{
	private String id;
	private String name;
	private String universalName;
	private String websiteUrl;
	private LICompanySearchResultLocations locations;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUniversalName() {
		return universalName;
	}
	public void setUniversalName(String universalName) {
		this.universalName = universalName;
	}
	public String getWebsiteUrl() {
		return websiteUrl;
	}
	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}
	public LICompanySearchResultLocations getLocations() {
		return locations;
	}
	public void setLocations(LICompanySearchResultLocations locations) {
		this.locations = locations;
	}
}

class LICompanySearchResultLocations{
	private int _total;
	private List<LICompanySearchResultLocation> values;
	
	public int get_total() {
		return _total;
	}
	public void set_total(int _total) {
		this._total = _total;
	}
	public List<LICompanySearchResultLocation> getValues() {
		return values;
	}
	public void setValues(List<LICompanySearchResultLocation> values) {
		this.values = values;
	}
}

class LICompanySearchResultLocation{
	private LICompanySearchResultAddress address;
	private LICompanySearchResultContactInfo contactInfo;
	
	public LICompanySearchResultAddress getAddress() {
		return address;
	}
	public void setAddress(LICompanySearchResultAddress address) {
		this.address = address;
	}
	public LICompanySearchResultContactInfo getContactInfo() {
		return contactInfo;
	}
	public void setContactInfo(LICompanySearchResultContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}
}

class LICompanySearchResultAddress{
	private String city;
	private String postalCode;
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	} 
}

class LICompanySearchResultContactInfo{}