package com.jobrapido.hack.rest.entities.twitter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class TwitterSearchResult {
	private String query;
	private String next_page;
	private List<TwitterSearchResultItem> results;
	
	public List<TwitterSearchResultItem> getTweets(){
		return results;
	}
	
	public List<TwitterSearchResultItem> getLastDayTweets(){
		Calendar calendar = new GregorianCalendar();
		Calendar calendarTwit = new GregorianCalendar();
		Date trialTime = new Date();
		calendar.setTime(trialTime);
		calendar.add(Calendar.HOUR, -24);
		
		List<TwitterSearchResultItem> lastWeekTweets = new ArrayList<TwitterSearchResultItem>();
		List<TwitterSearchResultItem> tweets = getTweets();
		
		for (TwitterSearchResultItem twitterSearchResultItem : tweets) {
			String created_at = twitterSearchResultItem.getCreated_at();
			Date d = new Date(created_at);
			calendarTwit.setTime(d);
			
			if(calendar.before(calendarTwit)){
				lastWeekTweets.add(twitterSearchResultItem);
			}
		}
		return lastWeekTweets;
	}

	public String getLastDayTweetsCount() {
		if(getLastDayTweets().size() == 15){
			return (next_page.length() > 0 ) ? ( "+" + String.valueOf(getLastDayTweets().size()) ) : ( String.valueOf(getLastDayTweets().size()) );
		}
		return String.valueOf(getLastDayTweets().size());
	}
}

class TwitterSearchResultItem{
	private String created_at;
	private String from_user;
	private String from_user_name;
	private String profile_image_url;
	private String text;
	
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getFrom_user() {
		return from_user;
	}
	public void setFrom_user(String from_user) {
		this.from_user = from_user;
	}
	public String getFrom_user_name() {
		return from_user_name;
	}
	public void setFrom_user_name(String from_user_name) {
		this.from_user_name = from_user_name;
	}
	public String getProfile_image_url() {
		return profile_image_url;
	}
	public void setProfile_image_url(String profile_image_url) {
		this.profile_image_url = profile_image_url;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
}