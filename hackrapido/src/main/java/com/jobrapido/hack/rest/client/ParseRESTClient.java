package com.jobrapido.hack.rest.client;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.sun.jersey.api.client.WebResource;

public class ParseRESTClient extends JerseyRESTClient {
	
	static Logger logger = Logger.getLogger(ParseRESTClient.class);
	
	private static final String BASE_API_URL 				= "https://api.parse.com";
	private static final String API_VERSION 				= "1";
	
	private static final String APP_ID 						= "auyN7tMq1undG8Zk7qQWevpaEYGQipzftESglz97";
	private static final String CLIENT_KEY 					= "wUjo51Z0FBk21Yx7m6mYGJWX4iNNGsQlr5YTFrZZ";
	
	private static final String REST_API_KEY 				= "QWPHim7ZD7k0sC0xDMfeTUmirLKBPKhjIm7sCgBi";
	private static final String MASTER_KEY 					= "IkUtZRuA5AnCTFMwzCEUZqRVJ3OzBpUue3smkrp9";
	
	private static final String APPLICATION_ID_HEADER_KEY 	= "X-Parse-Application-Id";
	private static final String REST_API_HEADER_KEY 		= "X-Parse-REST-API-Key";
	
	

	
	public String objCreate(String objSerialized, String className){
		
		WebResource r = c.resource(
				BASE_API_URL + URL_SEPARATOR +
				API_VERSION + URL_SEPARATOR +
				"classes"
				)
				.path(className);
		
		String response = r
				.type(MediaType.APPLICATION_JSON)
				.header(APPLICATION_ID_HEADER_KEY, APP_ID)
				.header(REST_API_HEADER_KEY, REST_API_KEY)
				.accept(MediaType.APPLICATION_JSON)
				.post(String.class, objSerialized);

		logger.info(response);
		
		return response;
	}
	
	
	public void objUpdate(String objSerialized, String objectId, String className){
		
		WebResource r = c.resource(
				BASE_API_URL + URL_SEPARATOR +
				API_VERSION + URL_SEPARATOR +
				"classes"
				)
				.path(className)
				.path(objectId);
		
		String response = r
				.type(MediaType.APPLICATION_JSON)
				.header(APPLICATION_ID_HEADER_KEY, APP_ID)
				.header(REST_API_HEADER_KEY, REST_API_KEY)
				.accept(MediaType.APPLICATION_JSON)
				.put(String.class, objSerialized);

		logger.info(response);
		
	}
	

	public String objDetails(String objId, String className){
		WebResource r = c.resource(
				BASE_API_URL + URL_SEPARATOR +
				API_VERSION + URL_SEPARATOR +
				"classes"
				)
				.path(className)
				.path(objId);
		
		String response = r
				.type(MediaType.APPLICATION_JSON)
				.header(APPLICATION_ID_HEADER_KEY, APP_ID)
				.header(REST_API_HEADER_KEY, REST_API_KEY)
				.accept(MediaType.APPLICATION_JSON)
				.get(String.class);
		
		logger.info(response);
		
		return response;
	}

	
	public String objsList(String className){
		WebResource r = c.resource(
				BASE_API_URL + URL_SEPARATOR +
				API_VERSION + URL_SEPARATOR +
				"classes"
				)
				.path(className);
		
		String response = r
				.type(MediaType.APPLICATION_JSON)
				.header(APPLICATION_ID_HEADER_KEY, APP_ID)
				.header(REST_API_HEADER_KEY, REST_API_KEY)
				.accept(MediaType.APPLICATION_JSON)
				.get(String.class);
		
//		logger.info(response);
		System.out.println(response);
		
		return response;
		
	}
	
	
	public static final void main(String[] args){
		ParseRESTClient client = new ParseRESTClient();
		System.out.println(client.objsList("jobseeker"));
	}


	private void delete(String className, String objectId) {
		WebResource r = c.resource(
				BASE_API_URL + URL_SEPARATOR +
				API_VERSION + URL_SEPARATOR +
				"classes"
				)
				.path(className)
				.path(objectId);
		
		String response = r
				.type(MediaType.APPLICATION_JSON)
				.header(APPLICATION_ID_HEADER_KEY, APP_ID)
				.header(REST_API_HEADER_KEY, REST_API_KEY)
				.accept(MediaType.APPLICATION_JSON)
				.delete(String.class);
		
		System.out.println(response);
	}
	
}
