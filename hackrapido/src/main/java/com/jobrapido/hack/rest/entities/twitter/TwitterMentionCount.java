package com.jobrapido.hack.rest.entities.twitter;

public class TwitterMentionCount {
	private String company;
	private String count;
	
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}	
}
