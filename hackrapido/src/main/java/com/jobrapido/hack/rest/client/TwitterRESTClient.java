package com.jobrapido.hack.rest.client;

import java.net.URLEncoder;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.WebResource;

public class TwitterRESTClient extends JerseyRESTClient {
	
//	private static final String TWITTER_API_BASE_URL 	= "https://api.twitter.com";
//	private static final String TWITTER_API_KEY 		= "qPUWsj58JCDMqDKHW3JwA";
//	private static final String TWITTER_SECRET_KEY 		= "TPi7A9W0VqvSbDHPGIm7Kau3hc7jT78gAF0GLtVbss";
	
	private static final String TWITTER_SEARCH_BASE_URL = "http://search.twitter.com/search.json";
	
//	Request token URL https://api.twitter.com/oauth/request_token
//	Authorize URL https://api.twitter.com/oauth/authorize
//	Access token URL https://api.twitter.com/oauth/access_token

//	private Token requestToken;
//	private Token accessToken;
//	private OAuthService service;

	@SuppressWarnings("deprecation")
	public String searchForTweetAbout(String searchTerm){
		WebResource r = c
				.resource(TWITTER_SEARCH_BASE_URL)
				.queryParam("q", URLEncoder.encode(searchTerm));
		
		String response = r.accept(
		        MediaType.APPLICATION_JSON_TYPE).
		        get(String.class);
		return response;
	}
	
	
	public String searchForTweetAboutHashTag(String searchTerm){
		return searchForTweetAbout("#"+searchTerm);
	}
	
	public String searchForTweetAboutMention(String searchTerm){
		return searchForTweetAbout("@"+searchTerm);
	}
	
	
}
