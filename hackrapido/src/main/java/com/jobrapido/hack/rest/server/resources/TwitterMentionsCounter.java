package com.jobrapido.hack.rest.server.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.jobrapido.hack.rest.utils.DataProvider;

@Path("/twitter-mentions-counter")
public class TwitterMentionsCounter {
	
	@GET
	@Path("{company}")
    @Produces("application/json")
    public String getTwitterMentionsCountLastWeek(@PathParam("company") String company) {
		return DataProvider.getTwitterMentionsCountLastWeek(company);
    }
}
