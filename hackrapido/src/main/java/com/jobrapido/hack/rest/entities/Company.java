package com.jobrapido.hack.rest.entities;

import java.util.ArrayList;
import java.util.List;

public class Company {

	private String name;
	private List<JobAdvert> adverts;

	public Company(String name) {
		this.setName(name);
		this.adverts = new ArrayList<JobAdvert>();
	}

	public void addJobAdvert(JobAdvert jobAdvert) {
		adverts.add(jobAdvert);
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
