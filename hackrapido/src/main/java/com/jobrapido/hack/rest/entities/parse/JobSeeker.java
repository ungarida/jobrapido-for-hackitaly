package com.jobrapido.hack.rest.entities.parse;

public class JobSeeker {
	String objectId;
	String name;
	String surname;
	String photo;
	String linkedinToken;
	String linkedinSecret;
	public String getObjectId() {
		return objectId;
	}
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getLinkedinToken() {
		return linkedinToken;
	}
	public void setLinkedinToken(String linkedinToken) {
		this.linkedinToken = linkedinToken;
	}
	public String getLinkedinSecret() {
		return linkedinSecret;
	}
	public void setLinkedinSecret(String linkedinSecret) {
		this.linkedinSecret = linkedinSecret;
	}
	
	
}
