package com.jobrapido.hack.rest.entities;

public class JobAdvert {
	
	private String id;
	private String title;
	private String description;
	private String location;
	private String company;
	private String date;
	private String website;
	private String url;
	
	
	
	public JobAdvert(){}

	


	public JobAdvert(String id, String title, String description,
			String location, String company, String date, String website,
			String url) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.location = location;
		this.company = company;
		this.date = date;
		this.website = website;
		this.url = url;
	}




	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getLocation() {
		return location;
	}



	public void setLocation(String location) {
		this.location = location;
	}



	public String getCompany() {
		return company;
	}



	public void setCompany(String company) {
		this.company = company;
	}



	public String getDate() {
		return date;
	}



	public void setDate(String date) {
		this.date = date;
	}



	public String getWebsite() {
		return website;
	}



	public void setWebsite(String website) {
		this.website = website;
	}



	public String getUrl() {
		return url;
	}



	public void setUrl(String url) {
		this.url = url;
	}
	
	
	
}
