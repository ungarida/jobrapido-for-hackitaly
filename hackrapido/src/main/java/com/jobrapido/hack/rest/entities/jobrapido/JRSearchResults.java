package com.jobrapido.hack.rest.entities.jobrapido;

import java.util.List;

public class JRSearchResults {
	String page;
	String pageItemsCount;
	Boolean hasNextPage;
	List<JRSearchResultItem> results;
	
	
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getPageItemsCount() {
		return pageItemsCount;
	}
	public void setPageItemsCount(String pageItemsCount) {
		this.pageItemsCount = pageItemsCount;
	}
	public Boolean getHasNextPage() {
		return hasNextPage;
	}
	public void setHasNextPage(Boolean hasNextPage) {
		this.hasNextPage = hasNextPage;
	}
	public List<JRSearchResultItem> getResults() {
		return results;
	}
	public void setResults(List<JRSearchResultItem> results) {
		this.results = results;
	}
	
	
}
