package com.jobrapido.hack.rest.server.resources;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.jobrapido.hack.rest.utils.DataProvider;


@Path("/company-search")
public class CompanySearch {

	@GET
	@Path("{name}")
    @Produces("application/json")
    public String searchLinkedinCompanies(
			@CookieParam("objectId") String objectId, 
			@PathParam("name") String name) {
		return DataProvider.searchLinkedinCompanies(objectId, name);
    }
	
	@GET
	@Path("/{location}/{name}")
    @Produces("application/json")
    public String searchLinkedinCompaniesNameLocations(
			@CookieParam("objectId") String objectId, 
			@DefaultValue("us") @PathParam("location") String location,
			@PathParam("name") String name) {
		
		return DataProvider.searchLinkedinCompanies(objectId, name, location);
    }
	
}
