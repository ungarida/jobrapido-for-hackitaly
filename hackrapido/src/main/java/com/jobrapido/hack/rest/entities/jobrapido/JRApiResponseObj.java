package com.jobrapido.hack.rest.entities.jobrapido;

public class JRApiResponseObj {
	
	private JRSearchParameters searchParameters;
	private JRSearchResults searchResults;
	
	
	public JRSearchParameters getSearchParameters() {
		return searchParameters;
	}
	public void setSearchParameters(JRSearchParameters searchParameters) {
		this.searchParameters = searchParameters;
	}
	public JRSearchResults getSearchResults() {
		return searchResults;
	}
	public void setSearchResults(JRSearchResults searchResults) {
		this.searchResults = searchResults;
	}
	
	
}
