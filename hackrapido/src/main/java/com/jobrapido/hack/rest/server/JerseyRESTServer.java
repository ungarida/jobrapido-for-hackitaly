package com.jobrapido.hack.rest.server;

import java.io.IOException;
import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.apache.log4j.Logger;
import org.glassfish.grizzly.http.Cookie;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;

import com.google.inject.Inject;
import com.jobrapido.hack.servlet.LinkedinOAuthHandler;
import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;

public class JerseyRESTServer {
	
	private final String BASE_URL = "http://hack.jobrapido.com/";
	private final String RESOURCE_PKG = "com.jobrapido.hack.rest.server.resources";
	private final URI BASE_URI = getBaseURI();
	
	
	@Inject LinkedinOAuthHandler linkedinOAuthHandler;
	
	static Logger logger = Logger.getLogger(JerseyRESTServer.class);
	
	public JerseyRESTServer(){}
	
	public void init(){
		HttpServer httpServer = null;
		logger.info("init");
		
		try {
			
			httpServer = startServer();
			
			/**
			 * Handle the login request here
			 * 
			 */
			httpServer.getServerConfiguration().addHttpHandler(
					new HttpHandler() {
						@Override
						public void service(Request req, Response res) throws Exception {
							logger.info("in auth request handler");
							
							String redirectToOAuthPage = linkedinOAuthHandler.getRedirectToOAuthPage();
							String requestToken = linkedinOAuthHandler.getRequestToken().toString();
							
							Cookie c = new Cookie("requestToken", requestToken);
							res.addCookie(c);
							
							Cookie c2 = new Cookie("objectId", linkedinOAuthHandler.getParseJobSeekerId());
							res.addCookie(c2);
							
							res.sendRedirect(redirectToOAuthPage);
							res.flush();
							
							
						}
					}, 
					"/authrequest/*");
			
			/**
			 * Handle the oauth callback
			 * 
			 */
			httpServer.getServerConfiguration().addHttpHandler(
					new HttpHandler() {
						@Override
						public void service(Request req, Response res) throws Exception {
							logger.info("in auth callback handler");
							String requestToken = null;
							String objectId = null;
							Cookie[] cookies = req.getCookies();
							for (Cookie cookie : cookies) {
								System.out.println(cookie.getValue());
								
								if(cookie.getName().equals("requestToken"))
									requestToken = cookie.getValue().toString();
								
								if(cookie.getName().equals("objectId"))
									objectId = cookie.getValue().toString();
								
							}
							String verifier = req.getParameter("oauth_verifier");
							
							String oAuthHandleCallback = linkedinOAuthHandler.oAuthHandleCallback(verifier, requestToken, objectId);
							Cookie authorizationCookie = new Cookie("authorization", "true");
							res.addCookie(authorizationCookie);
							res.sendRedirect(oAuthHandleCallback);
							res.flush();
							
						}
					}, 
					"/authcallback/*");
			
		} catch (IOException e) {
			logger.error("some error starting grizzly server", e);
		}
		
		logger.info("Jersey app started..\nHit enter to stop it...");
        
        try {
			System.in.read();
		} catch (IOException e) {
			logger.error("some error reading from console", e);
		}
        
        httpServer.stop();
	}
	
	private int getPort(int defaultPort) {
        String port = System.getProperty("jersey.test.port");
        if (null != port) {
            try {
                return Integer.parseInt(port);
            } catch (NumberFormatException e) {
            }
        }
        return defaultPort;        
    } 
    
    private URI getBaseURI() {
        return UriBuilder.fromUri(BASE_URL).port(getPort(10000)).build();
    }

    

    protected HttpServer startServer() throws IOException {
        logger.info("Starting grizzly...");
        ResourceConfig rc = new PackagesResourceConfig(RESOURCE_PKG);
        return GrizzlyServerFactory.createHttpServer(BASE_URI, rc);
    }
}
