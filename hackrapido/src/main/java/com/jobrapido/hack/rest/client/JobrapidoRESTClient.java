package com.jobrapido.hack.rest.client;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.WebResource;

public class JobrapidoRESTClient extends JerseyRESTClient {
	
	private final static String BASE_API_URL = "http://us.jobrapido.com";
	private final static String JR_API_USER = "hackitaly";
	private final static String JR_API_PWD = "h4ck-m3";
	private static final String DEFAULT_LOCATION = "us";
	
	public JobrapidoRESTClient(){
		super();
	}
	
	public String searchJobAdvertsWhatLocation(String location, String what) {
		if (DEFAULT_LOCATION.equals(location)) { location = ""; }
		WebResource r = c
				.resource(BASE_API_URL + URL_SEPARATOR)
				.queryParam("do", "api.feed")
				.queryParam("login", JR_API_USER)
				.queryParam("password", JR_API_PWD)
				.queryParam("l", location)
				.queryParam("w", what)
				.queryParam("format", "json");
		
		String response = r.accept(
		        MediaType.APPLICATION_JSON_TYPE).
		        get(String.class);
		
		return response;
	}
}
