package com.jobrapido.hack.rest.server.resources;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.commons.lang3.StringUtils;

@Path("/isauthorized")
public class AuthorizationChecker {
	
	@GET
	@Produces("application/json")
	public String getMyLinkedinProfileInfo(
			@CookieParam("authorization") String authorization) {
		Boolean result = StringUtils.isNotEmpty(authorization) && "true".equals(authorization);
		return "{\"authorization\":"+result+"}";
	}
	
}
