package com.jobrapido.hack.rest.entities.linkedin;

import java.util.ArrayList;
import java.util.List;


public class LIUser {
	private String id;
	private String firstName;
	private String lastName;
	private String headline;
	private String pictureUrl;
	private String publicProfileUrl;
	private String industry;
	private Location location;
	private Skills skills;
	private PhoneNumbers phoneNumbers;

	
	public boolean hasSkills(){
		return getSkills().get_total() > 0;
	}
	
	public List<String> getSkillsList(){
		List<String> skillsList = new ArrayList<String>();
		List<Skill> values = getSkills().getValues();
		for (Skill skill : values) {
			skillsList.add(skill.getSkill().getName());
		}
		return skillsList;
	}
	
	public boolean hasPhones(){
		return getPhoneNumbers().get_total() > 0;
	}
	
	public List<String> getPhonesList(){
		List<String> phonesList = new ArrayList<String>();
		List<PhoneNumber> values = getPhoneNumbers().getValues();
		for (PhoneNumber phoneNumber : values) {
			phonesList.add(phoneNumber.getPhoneNumber());
		}
		return phonesList;
	}
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getHeadline() {
		return headline;
	}
	public void setHeadline(String headline) {
		this.headline = headline;
	}
	public String getPictureUrl() {
		return pictureUrl;
	}
	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}
	public String getPublicProfileUrl() {
		return publicProfileUrl;
	}
	public void setPublicProfileUrl(String publicProfileUrl) {
		this.publicProfileUrl = publicProfileUrl;
	}
	public String getLocation() {
		return location.getName();
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public Skills getSkills() {
		return skills;
	}
	public void setSkills(Skills skills) {
		this.skills = skills;
	}
	public PhoneNumbers getPhoneNumbers() {
		return phoneNumbers;
	}
	public void setPhoneNumbers(PhoneNumbers phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
	
}


class Location{
	private String name;
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}
}

class PhoneNumbers{
	private int _total;
	private List<PhoneNumber> values;
	
	public int get_total() {
		return _total;
	}
	public void set_total(int _total) {
		this._total = _total;
	}
	public List<PhoneNumber> getValues() {
		return values;
	}
	public void setValues(List<PhoneNumber> values) {
		this.values = values;
	}
}

class PhoneNumber{
	private String phoneNumber;
	private String phoneType;
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPhoneType() {
		return phoneType;
	}
	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}
}

class Skills{
	private int _total;
	private List<Skill> values;
	public int get_total() {
		return _total;
	}
	public void set_total(int _total) {
		this._total = _total;
	}
	public List<Skill> getValues() {
		return values;
	}
	public void setValues(List<Skill> values) {
		this.values = values;
	}
}

class Skill{
	private String id;
	private SkillName skill;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public SkillName getSkill() {
		return skill;
	}
	public void setSkill(SkillName skill) {
		this.skill = skill;
	}
}

class SkillName{
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
