package com.jobrapido.hack.rest;

import com.google.inject.AbstractModule;
import com.jobrapido.hack.rest.utils.DataProvider;

public class HackItalyRESTModule extends AbstractModule {

	@Override
	protected void configure() {
		requestStaticInjection(DataProvider.class);
		
	}

}
