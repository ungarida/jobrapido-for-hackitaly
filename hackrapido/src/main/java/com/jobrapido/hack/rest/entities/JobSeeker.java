package com.jobrapido.hack.rest.entities;

import java.util.List;

import com.jobrapido.hack.rest.entities.linkedin.LIUser;

public class JobSeeker {
	private Long id;
	
	// General info
	private String name;
	private String surname;
	private String email;
	private String photo;
	private String headline;
	private String industry;
	private String location;
	private List<String> phones;
	private List<String> skills;
	
	public JobSeeker(LIUser user){
		this.name = user.getFirstName();
		this.surname = user.getLastName();
		this.photo = user.getPictureUrl();
		this.headline = user.getHeadline();
		this.industry = user.getIndustry();
		this.location = user.getLocation();
		
		if(user.hasSkills()){
			this.setSkills(user.getSkillsList());
		}
		
		if(user.hasPhones()){
			this.setPhones(user.getPhonesList());
		}
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getHeadline() {
		return headline;
	}
	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public List<String> getSkills() {
		return skills;
	}

	public void setSkills(List<String> skills) {
		this.skills = skills;
	}

	public List<String> getPhones() {
		return phones;
	}

	public void setPhones(List<String> phones) {
		this.phones = phones;
	}
		
	
}
