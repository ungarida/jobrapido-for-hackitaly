package com.jobrapido.hack.rest.client;

import java.net.URLEncoder;

import org.apache.log4j.Logger;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.LinkedInApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import com.google.inject.Inject;
import com.jobrapido.hack.rest.entities.parse.JobSeeker;
import com.jobrapido.hack.rest.utils.JsonSerializer;
import com.jobrapido.hack.servlet.LinkedinOAuthHandler;
import com.sun.jersey.api.client.WebResource;

public class LinkedinRESTClient extends JerseyRESTClient {
	
	static Logger logger = Logger.getLogger(LinkedinRESTClient.class);
	
	private static final String API_BASE_URL 			= "https://api.linkedin.com";
	private static final String API_VERSION 			= "v1";
	
	private Token accessToken;
	private OAuthService service;
	
	@Inject private ParseRESTClient parseRESTClient;
	
	public String getMyLinkedinProfileInfo(String objectId){
		logger.info("********getMyLinkedinProfileInfo********");
		setAccessTokenFromParseUser(objectId);
		String profileFieldsRequested = ":(id,first-name,last-name,headline,picture-url,location:(name),industry,public-profile-url,phone-numbers,skills)";
        String url = "http://api.linkedin.com/v1/people/~";
        OAuthRequest request = new OAuthRequest(Verb.GET, url+profileFieldsRequested);
        request.addHeader("x-li-format", "json");
        service.signRequest(accessToken, request);
        Response response = request.send();
        logger.info(response.getBody());
		return response.getBody();
	}
	
	public String userProfileSearchByNameAndSurname(String objectId, String name, String surname){
		logger.info("********userProfileSearch********");
		setAccessTokenFromParseUser(objectId);
		WebResource r = c
				.resource(API_BASE_URL)
				.path(API_VERSION)
				.path("people-search")
				.queryParam("first-name", name)
				.queryParam("last-name", surname);
		String url = r.toString();
        OAuthRequest request = new OAuthRequest(Verb.GET, url);
        request.addHeader("x-li-format", "json");
        service.signRequest(accessToken, request);
        Response response = request.send();
        logger.info(response.getBody());
		return response.getBody();
	}
	
	public String companiesProfileSearchByName(String objectId, String companyName){
		logger.info("********companyProfileSearchByName********");
		setAccessTokenFromParseUser(objectId);
		WebResource r = c
				.resource(API_BASE_URL)
				.path(API_VERSION)
				.path("company-search");
		String profileFieldsRequested = ":(companies:(id,name,universal-name,website-url,locations))";
		
		String searchQueryPart = "?keywords="+URLEncoder.encode(companyName)+"&order=relevance";
		String url = r.toString() + profileFieldsRequested + searchQueryPart;
        OAuthRequest request = new OAuthRequest(Verb.GET, url);
        request.addHeader("x-li-format", "json");
        service.signRequest(accessToken, request);
        Response response = request.send();
        logger.info(response.getBody());
		return response.getBody();
	}
	

	public String companyProfileSearchByNameAndLocation(String objectId, String name,
			String location) {
		logger.info("********companyProfileSearchByNameAndLocation********");
		setAccessTokenFromParseUser(objectId);
		WebResource r = c
				.resource(API_BASE_URL)
				.path(API_VERSION)
				.path("company-search")
				.queryParam("name", name)
				.queryParam("locations", location);
		String url = r.toString();
        OAuthRequest request = new OAuthRequest(Verb.GET, url);
        request.addHeader("x-li-format", "json");
        service.signRequest(accessToken, request);
        Response response = request.send();
        logger.info(response.getBody());
		return response.getBody();
	}	

	
	public String getCompanyProfileById(String objectId, String id) {
		logger.info("********getCompanyProfileById********");
		if (id!=null && !"".equals(id)) {
			setAccessTokenFromParseUser(objectId);
			WebResource r = c
					.resource(API_BASE_URL)
					.path(API_VERSION)
					.path("companies")
					.path(id);
			String profileFieldsRequested = ":(id,name,universal-name,website-url,company-type,square-logo-url,employee-count-range,locations:(description,is-headquarters,address),stock-exchange,num-followers,twitter-id,blog-rss-url)";
			String url = r.toString() + profileFieldsRequested;
	        OAuthRequest request = new OAuthRequest(Verb.GET, url);
	        request.addHeader("x-li-format", "json");
	        service.signRequest(accessToken, request);
	        Response response = request.send();
	        logger.info(response.getBody());
			return response.getBody();
		} else {
			return null;
		}
	}
	
	
	
	// private methods
	private void setAccessTokenFromParseUser(String objectId){
		// build the service to make the access token request and all other requests
		service = new ServiceBuilder()
            .provider(LinkedInApi.class)
            .apiKey(LinkedinOAuthHandler.LINKEDIN_API_KEY)
            .apiSecret(LinkedinOAuthHandler.LINKEDIN_SECRET_KEY)
            .build();
		// get token and secret for user on parse with a given objectId
		String objDetails = parseRESTClient.objDetails(objectId, "jobseeker");
		JobSeeker fromParseJobSeeker = JsonSerializer.fromParseJobSeeker(objDetails);
		// set the access token
		accessToken = new Token(fromParseJobSeeker.getLinkedinToken(), fromParseJobSeeker.getLinkedinSecret());
	}

}
