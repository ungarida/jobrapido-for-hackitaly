package com.jobrapido.hack.rest.server.resources;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.jobrapido.hack.rest.utils.DataProvider;

@Path("/company")
public class Company {
	
	@GET
	@Path("{l}")
    @Produces("application/json")
    public String searchCompaniesLocationWhat(@PathParam("l") @DefaultValue("us") String location) {
		return DataProvider.getCompaniesFromJobrapido(location, "");
    }
	
	@GET
	@Path("{l}/{w}")
    @Produces("application/json")
    public String searchCompaniesLocationWhat(@PathParam("l") @DefaultValue("us") String location, @PathParam("w") String what) {
		return DataProvider.getCompaniesFromJobrapido(location, what);
    }
	
}
