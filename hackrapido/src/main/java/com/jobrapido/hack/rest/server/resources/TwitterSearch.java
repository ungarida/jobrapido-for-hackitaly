package com.jobrapido.hack.rest.server.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.jobrapido.hack.rest.utils.DataProvider;

@Path("/twitter-search")
public class TwitterSearch {
	
	@GET
	@Path("{searchTerm}")
    @Produces("application/json")
    public String searchCompaniesLocationWhat(@PathParam("searchTerm") String searchTerm) {
		return DataProvider.searchTwitterForTerm(searchTerm);
    }
}
