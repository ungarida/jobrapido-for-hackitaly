package com.jobrapido.hack.servlet;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.LinkedInApi;
import org.scribe.model.Verifier;

import com.jobrapido.hack.rest.entities.parse.JobSeeker;
import com.jobrapido.hack.rest.entities.parse.ObjectCreatedResponse;
import com.jobrapido.hack.rest.utils.JsonSerializer;

public class LinkedinOAuthHandler extends BaseOAuthHandler {
	
	public static final String LINKEDIN_API_KEY 		= "8eu5v3ke3nrm";
	public static final String LINKEDIN_SECRET_KEY 	= "nbhc5cZtWmL99per";
//	public static final String LINKEDIN_API_KEY 		= "wnvo9j2w0n07";
//	public static final String LINKEDIN_SECRET_KEY 	= "52NA2jjIe3OqfvBn";
	
	private String parseJobSeekerId;
	
	public String getRedirectToOAuthPage(){
		// build the service to make the access token request and all other requests
		service = new ServiceBuilder()
            .provider(LinkedInApi.class)
            .apiKey(LINKEDIN_API_KEY)
            .apiSecret(LINKEDIN_SECRET_KEY)
            .callback("http://hack.jobrapido.com/api/authcallback")
            .build();
		
		// get our request token
		requestToken = service.getRequestToken();
		
		JobSeeker js = new JobSeeker();
		String objSerialized = JsonSerializer.toParseJobSeeker(js);
		String objCreatedString = parseRESTClient.objCreate(objSerialized, "jobseeker");
		ObjectCreatedResponse objectCreatedResponse = JsonSerializer.fromParseObjectCreated(objCreatedString);
		setParseJobSeekerId(objectCreatedResponse.getObjectId());
		return service.getAuthorizationUrl(requestToken);
	}
	
	public String oAuthHandleCallback(String verifierString, String requestTokenString, String objectId){
		// build the verifer with the pin the user enters
		verifier = new Verifier(verifierString);
		
		// use the request token and our verifier to get an access token and we are all set
		accessToken = service.getAccessToken(requestToken, verifier);
		System.out.println(accessToken);
		
		JobSeeker js = new JobSeeker();
		js.setLinkedinToken(accessToken.getToken());
		js.setLinkedinSecret(accessToken.getSecret());
		String objSerialized = JsonSerializer.toParseJobSeeker(js);

		parseRESTClient.objUpdate(objSerialized, objectId, "jobseeker");
		
		return WEBSITE_SEARCH_URL;
	}

	public String getParseJobSeekerId() {
		return parseJobSeekerId;
	}

	public void setParseJobSeekerId(String parseJobSeekerId) {
		this.parseJobSeekerId = parseJobSeekerId;
	}

	
}
