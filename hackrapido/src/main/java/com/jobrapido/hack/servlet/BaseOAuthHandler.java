package com.jobrapido.hack.servlet;

import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import com.google.inject.Inject;
import com.jobrapido.hack.rest.client.ParseRESTClient;

public class BaseOAuthHandler {

	@Inject protected ParseRESTClient parseRESTClient;

	protected static final String WEBSITE_BASE_URL = "http://hack.jobrapido.com";
	protected static final String WEBSITE_SEARCH_URL = "http://hack.jobrapido.com/#search";
	
	protected Token requestToken;
	protected Token accessToken;
	protected OAuthService service;
	protected Verifier verifier;
	
	
	public Token getRequestToken() {
		return requestToken;
	}
	public void setRequestToken(Token requestToken) {
		this.requestToken = requestToken;
	}
	public Token getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(Token accessToken) {
		this.accessToken = accessToken;
	}
	public OAuthService getService() {
		return service;
	}
	public void setService(OAuthService service) {
		this.service = service;
	}
	public Verifier getVerifier() {
		return verifier;
	}
	public void setVerifier(Verifier verifier) {
		this.verifier = verifier;
	}
	
	
}
